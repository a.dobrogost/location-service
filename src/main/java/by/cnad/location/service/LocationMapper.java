package by.cnad.location.service;

import by.cnad.location.entity.Location;
import org.springframework.stereotype.Component;

@Component
public class LocationMapper {

  public LocationDto toDto(Location location) {
    return LocationDto.builder()
        .id(location.getId())
        .name(location.getName())
        .address(location.getAddress())
        .country(location.getCountry())
        .city(location.getCity())
        .companyId(location.getCompanyId())
        .build();
  }
}
